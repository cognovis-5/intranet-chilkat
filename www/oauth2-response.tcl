ad_page_contract {
    oauth2 Reponse file which should recieve the return from the oauth2 provider for further processing
} {
    code
    state
} 

# Get the user_id from the state, which actually is the api_token
set user_id [cog_rest::authentication::bearer_auth -header_token $state]

if {$user_id ne ""} {
    set response_status_code [intranet_chilkat::office365::parse_tenant_consent -code $code -user_id $user_id]
} 

if {$err_msg ne ""} {
    ad_return_error "Impossible to login" "$err_msg"    
} else {
    set redirect_url [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365RedirectUrl"]

    ad_returnredirect $redirect_url
}


