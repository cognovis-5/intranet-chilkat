ad_page_contract {

    Edit the users email configuration

    @author malte.sussdorff@cognovis.de
} {
    {user_id ""}
}

if {$user_id eq ""} {
    set user_id [auth::get::user_id]
}

# TODO - Actual provide a page which allows to configure those values
if {0} {
 smtp_auth_type_id | integer | default 86500
 smtpuser          | text    |
 smtppassword      | text    |
 smtphost          | text    |
 smtpport          | integer |
 imaphost          | text    |
 imapport          | integer |
 imapuser          | text    |
 imappassword      | text    |
 imapsentfolder

}