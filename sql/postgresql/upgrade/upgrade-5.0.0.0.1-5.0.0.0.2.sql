-- 
-- packages/intranet-chilkat/sql/postgresql/upgrade/upgrade-5.0.0.0.1-5.0.0.0.2.sql
-- 
-- Copyright (c) 2020, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- @author <yourname> (<your email>)
-- @creation-date 2012-01-06
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-chilkat/sql/postgresql/upgrade/upgrade-5.0.0.0.1-5.0.0.0.2.sql','');


create table users_office365 (
    user_id integer
        constraint users_office365_pk
        primary key
        constraint users_office365_pk_fk
        references users,
    refresh_token text,
    access_token text,
    id_token text
);