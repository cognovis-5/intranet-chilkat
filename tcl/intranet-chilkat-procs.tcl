# Chilkat

ad_library {
	Service procedures to work with chilkat

	@author malte.sussdorff@cognovis.de
}

namespace eval intranet_chilkat {}

ad_proc -public intranet_chilkat::ssh_exec {
	-chilkat_ssh
	-command
} {
	Execute a command over an SSH connection

	@return String send back from the SSH connection
} {

	set channelNum [CkSsh_OpenSessionChannel $chilkat_ssh]
	cog_log Notice "SendReq [CkSsh_SendReqExec $chilkat_ssh $channelNum $command]"
	
	cog_log Notice "Receivetoclose [CkSsh_ChannelReceiveToClose $chilkat_ssh $channelNum]"                
	set received_text "[CkSsh_getReceivedText $chilkat_ssh $channelNum "ansi"]"
	ds_comment "$received_text"
	return $received_text
}

ad_proc -public intranet_chilkat::ssh_connect {
	-server
	-user
	{-port ""}
	-private_key
} {
	Check if the SSH Connection works. Return 0 if it doesn't, otherwise return the SSH Channel
} {
	set sshkey [new_CkSshKey]
	
	#  First, use the LoadText convenience method to load a text file (of any SSH key format)
	#  into a string:
	set strKey [CkSshKey_loadText $sshkey $private_key]
	
	#  Next, load the key into the SSH key object.
	set success [CkSshKey_FromOpenSshPrivateKey $sshkey $strKey]

	if {[expr $success != 1]} then {
		cog_log Error "[CkSshKey_lastErrorText $sshkey]"
		return 0
	} else {
		cog_log Debug "Loaded OpenSSH private key."
	}
	
	set ssh [new_CkSsh]
	set success [CkSsh_UnlockComponent $ssh [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]
	
	if {[expr $success != 1]} then {
		cog_log Error "INVALID LICENSE:: [CkSshKey_lastErrorText $sshkey]"
		return 0
	}
	
	#  Set some timeouts, in milliseconds:
	CkSsh_put_ConnectTimeoutMs $ssh 5000
	CkSsh_put_IdleTimeoutMs $ssh 120000
	
	set success [CkSsh_Connect $ssh $server $port]
	if {[expr $success != 1]} then {
		cog_log Error "Can't connect to server:: [CkSshKey_lastErrorText $sshkey]"
		return 0
	}
	set success [CkSsh_AuthenticatePk $ssh $user $sshkey]
	if {[expr $success != 1]} then {
		cog_log Error "Can't authenticate:: [CkSshKey_lastErrorText $sshkey]"
		return 0
	}
	ds_comment "Successful connection : $ssh"
	delete_CkSshKey $sshkey

	return $ssh
} 

# Get the version of the Chilkat library
ad_proc -public intranet_chilkat::version {
} {
	Return the version of the Chilkat library
} {
	set glob [new_CkGlobal]

	return [CkGlobal_version $glob]
}

# ---------------------------------------------------------------
# ZIP Functions
# ---------------------------------------------------------------
ad_proc -public intranet_chilkat::create_zip {
	{-filesystem_files ""}
	{-directories ""}
	{-extension ""}
	-zipfile
} {
	Zips the files in filesystem_files into the zip_file

	@param filesystem_files List of files with full path which are to be zipped
	@param zipfile Filepath of the zipfile
	@param directories Directories to zip up
	@param extension If directories is provided, only zip up the files matching the extension ala *\$extension
	@return zipfile path if successful, empty string if not
} {
	set zip [new_CkZip]
	set success [CkZip_UnlockComponent $zip [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]

	if {[expr $success != 1]} then {
		cog_log Error [CkZip_lastErrorText $zip]
		delete_CkZip $zip
		return ""
		exit
	}

	#  The NewZip method only initializes the Zip object -- it does
	#  not create or write a .zip file.
	set success [CkZip_NewZip $zip "$zipfile"]
	if {[expr $success != 1]} then {
		cog_log Error "[CkZip_lastErrorText $zip]"
		delete_CkZip $zip
		return ""
		exit
	}

	#  Add a reference to a file.  This is the file that will
	#  be added to a pre-existing .zip archive.
	#  Note: this does not read or compress the file contents --
	#  it simply adds a reference to the zip object.

	set saveExtraPath 0
	foreach file $filesystem_files {
		set success [CkZip_AppendOneFileOrDir $zip $file $saveExtraPath]
		if {[expr $success != 1]} then {
			cog_log Error "[CkZip_lastErrorText $zip]"
			delete_CkZip $zip
			return ""
			exit
		}
	}

	set recurse 1
	foreach dir $directories {
		if {$extension eq ""} {
			set success [CkZip_AppendFiles $zip "$dir/*" $recurse]
		} else {
			set success [CkZip_AppendFiles $zip "$dir/*$extension" 0]
		}

		if {[expr $success != 1]} then {
			cog_log Error [CkZip_lastErrorText $zip]
			delete_CkZip $zip
			return ""
			exit
		}
	}

	set success [CkZip_WriteZipAndClose $zip]
	if {[expr $success != 1]} then {
		cog_log Error "[CkZip_lastErrorText $zip]"
		delete_CkZip $zip
		return ""
		exit
	}
	
	return "$zipfile"
	delete_CkZip $zip
}

ad_proc -public intranet_chilkat::unzip_extension {
	-zipfile
	-extension
} {
	Unzips the files in zipfile of the extension. Ignore paths

	@param zipfile Filepath of the zipfile
	@return list of unzip files, empty string if not
} {
	set zip [new_CkZip]
	set success [CkZip_UnlockComponent $zip [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]

	if {[expr $success != 1]} then {
		puts [CkZip_lastErrorText $zip]
		delete_CkZip $zip
		return ""
		exit
	}
	
	set success [CkZip_OpenZip $zip $zipfile]
	if {[expr $success != 1]} then {
		puts [CkZip_lastErrorText $zip]
		delete_CkZip $zip
		exit
	}
	
	#  Returns the number of files and directories unzipped.
	#  Unzips to /my_files, re-creating the directory tree
	#  from the .zip.
	
	set unzip_dir [ns_tmpnam]
	exec /bin/mkdir "$unzip_dir"
	
	set unzipCount [CkZip_UnzipMatchingInto $zip $unzip_dir "*$extension" 1]
	if {[expr $unzipCount < 0]} then {
		cog_log Error "[CkZip_lastErrorText $zip]"
	}

	return "[glob -directory $unzip_dir "*.sdltb"]"
	delete_CkZip $zip
}

ad_proc -public intranet_chilkat::unzip_files {
    -zipfile
    {-dest_path}
} {
    Unzips the files in zipfile. Ignores any control files and directory structure
    
    @param zipfile Filepath of the zipfile
    @param dest_path Folder into which to copy the files after zipping
    @return list of unzip files, empty string if not
} {
    set zip [new_CkZip]
    set success [CkZip_UnlockComponent $zip [parameter::get_from_package_key -package_key intranet-chilkat -parameter "ChilkatLicense"]]
    
    if {[expr $success != 1]} then {
	puts [CkZip_lastErrorText $zip]
	delete_CkZip $zip
	return ""
	exit
    }
    
    set success [CkZip_OpenZip $zip $zipfile]
    if {[expr $success != 1]} then {
	puts [CkZip_lastErrorText $zip]
	delete_CkZip $zip
	exit
    }
	
    #  Returns the number of files and directories unzipped.
    #  Unzips to /my_files, re-creating the directory tree
    #  from the .zip.
    
    set unzip_dir [ns_tmpnam]
    exec /bin/mkdir "$unzip_dir"
	
    set unzipCount [CkZip_UnzipInto $zip $unzip_dir]
    if {[expr $unzipCount < 0]} then {
	cog_log Error "[CkZip_lastErrorText $zip]"
    }

    # Remove unwanted files
    foreach control_file [glob -nocomplain -directory $unzip_dir -types {f c b} ".*"] {
	file delete $control_file
    }

    if {$dest_path ne ""} {
	# Move the files
	foreach unzipped_file [glob -nocomplain -directory $unzip_dir -types f "*"] {
	    file rename -force $unzipped_file $dest_path
	}
    } else {
	set dest_path $unzip_dir
    }
    
    return "[glob -directory $dest_path "*"]"
    delete_CkZip $zip
}

# ---------------------------------------------------------------
# XML Functions
# ---------------------------------------------------------------
ad_proc -public intranet_chilkat::array_to_xml {
	-xml_name
	-array_name
} {
	Converts an array into an XML object with the Tag being the array name and the array key/values the Children of the tag
	@param xml_name Name of the XML Object to work with. Will be created, should be destroyed after use
	@param array_name Name of the array in the calling environment from which to get the tag an key/values
} {
	upvar $xml_name localXml
	upvar $array_name localArray

	set localXml [new_CkXml]
	CkXml_put_Tag $localXml $array_name

	foreach {key val} [array get localArray] {
		CkXml_NewChild $localXml $key $val
	}
	CkXml_GetRoot2 $localXml
}