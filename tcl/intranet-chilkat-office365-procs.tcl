
ad_library {
	Service procedures to work with chilkat and office 365

	@author malte.sussdorff@cognovis.de
}

namespace eval intranet_chilkat::office365 {
    
    ad_proc -public oauth2_access_url {

    } {
        Register the token?
    } {
        set client_secret [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientSecret"]
        set client_id [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientId"]
        set authorization_endpoint [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365OauthAuthorizationEndpoint"]

        set user_id [auth::require_login]

        set auto_login [im_generate_auto_login -user_id $user_id]
        set api_key [base64::encode "${user_id}:$auto_login"]

        set sbUrl [new_CkStringBuilder]

        CkStringBuilder_Append $sbUrl "${authorization_endpoint}?"
        CkStringBuilder_Append $sbUrl "response_type=code&"
        CkStringBuilder_Append $sbUrl "state=${api_key}&"
        CkStringBuilder_Append $sbUrl "client_id=${client_id}&"

        # Provide a SPACE separated list of scopes.
        # Important: The offline_access scope is needed to get a refresh token.
        set sbScope [new_CkStringBuilder]
        set scope [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365Scope"]
        CkStringBuilder_Append $sbScope $scope
        CkStringBuilder_Append $sbUrl "scope="
        CkStringBuilder_Append $sbUrl "[CkStringBuilder_getEncoded $sbScope "url" "utf-8"]&"

        set sbRedirectUri [new_CkStringBuilder]
        set redirect_uri [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365RedirectUri"]
        if {$redirect_uri eq ""} {
            set redirect_uri "[ad_url]/intranet-chilkat/oauth2-response"            
        } else {
            set redirect_uri "${redirect_uri}/intranet-chilkat/oauth2-response"
        }        
        CkStringBuilder_Append $sbRedirectUri $redirect_uri
        CkStringBuilder_Append $sbUrl "redirect_uri="
        CkStringBuilder_Append $sbUrl [CkStringBuilder_getEncoded $sbRedirectUri "url" "utf-8"]        

        return [CkStringBuilder_getAsString $sbUrl]
    }

    ad_proc parse_tenant_consent {
        -code:required
        -user_id:required
    } {
        Parses the tenant parse_tenant_consent

        @param code string String to use to call back to microsoft

        @return response_status_code
    } {
        set token_endpoint [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365OauthTokenEndpoint"]
        set client_secret [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientSecret"]
        set client_id [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientId"]

        set http [new_CkHttp]
        set req [new_CkHttpRequest]
        CkHttpRequest_put_HttpVerb $req "POST"
        CkHttpRequest_put_Path $req $token_endpoint
        
        set redirect_uri [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365RedirectUri"]
        if {$redirect_uri eq ""} {
            set redirect_uri "[ad_url]/intranet-chilkat/oauth2-response"            
        } else {
            set redirect_uri "${redirect_uri}/intranet-chilkat/oauth2-response"
        }        

        CkHttpRequest_AddParam $req "client_id" "$client_id"        
        CkHttpRequest_AddParam $req "redirect_uri" "$redirect_uri"
        CkHttpRequest_AddParam $req "client_secret" "$client_secret"        
        CkHttpRequest_AddParam $req "grant_type" "authorization_code"
        CkHttpRequest_AddParam $req "code" "$code"

        set resp [CkHttp_PostUrlEncoded $http $token_endpoint $req]
        if {[CkHttp_get_LastMethodSuccess $http] == 0} then {
            puts [CkHttp_lastErrorText $http]
            delete_CkHttp $http
            delete_CkHttpRequest $req
            exit
        }

        set response_status_code "[CkHttpResponse_get_StatusCode $resp]"
        upvar err_msg err_msg
        if {![info exists err_msg]} {
            set err_msg ""
        }

        set jsonResp [new_CkJsonObject]
        CkJsonObject_Load $jsonResp [CkHttpResponse_bodyStr $resp]

        switch $response_status_code {
            401 {
                append err_msg "[CkJsonObject_stringOf $jsonResp "error"]:<p/> [CkJsonObject_stringOf $jsonResp "error_description"]"
                cog_log Error "Office365 Login failed - Parse Tenant Consent Response body: [CkHttpResponse_bodyStr $resp]"
            }
            default {
                set access_token "[CkJsonObject_stringOf $jsonResp "access_token"]"
                set refresh_token "[CkJsonObject_stringOf $jsonResp "refresh_token"]"
                set id_token "[CkJsonObject_stringOf $jsonResp "id_token"]"
                
                set update_p [db_string update_p "select 1 from users_office365 where user_id = :user_id" -default 0]
                if {$update_p} {
                    # Update
                    db_dml update_access_token "update users_office365 set access_token = :access_token, refresh_token=:refresh_token, id_token=:id_token where user_id = :user_id"
                } else {
                    db_dml insert_token "insert into users_office365 (user_id,access_token,refresh_token,id_token) values (:user_id,:access_token,:refresh_token,:id_token)"
                }
            }

        }

        delete_CkHttpResponse $resp
        delete_CkHttp $http
        delete_CkHttpRequest $req

        return $response_status_code
    }

     ad_proc refresh_token {
        -user_id:required
    } {
        Refresh the token for the user
    } {
        set token_endpoint [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365OauthTokenEndpoint"]
        set client_secret [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientSecret"]
        set client_id [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365ClientId"]
        set scope [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365Scope"]
        set refresh_token [db_string refresh_token "select refresh_token from users_office365 where user_id = :user_id" -default ""]

        if {$refresh_token ne ""} {
            set http [new_CkHttp]
            set req [new_CkHttpRequest]
            CkHttpRequest_put_HttpVerb $req "POST"
            CkHttpRequest_put_Path $req $token_endpoint
            
            set redirect_uri [parameter::get_from_package_key -package_key intranet-chilkat -parameter "Office365RedirectUri"]
            if {$redirect_uri eq ""} {
                set redirect_uri "[ad_url]/intranet-chilkat/oauth2-response"            
            } else {
                set redirect_uri "${redirect_uri}/intranet-chilkat/oauth2-response"
            }        

            CkHttpRequest_AddParam $req "client_id" "$client_id"
            CkHttpRequest_AddParam $req "scope" "$scope"
            CkHttpRequest_AddParam $req "refresh_token" "$refresh_token"
            CkHttpRequest_AddParam $req "client_secret" "$client_secret"        
            CkHttpRequest_AddParam $req "grant_type" "refresh_token"
        
            set resp [CkHttp_PostUrlEncoded $http $token_endpoint $req]
            if {[CkHttp_get_LastMethodSuccess $http] == 0} then {
                cog_log Error [CkHttp_lastErrorText $http]
                delete_CkHttp $http
                delete_CkHttpRequest $req
                exit
            }

            set jsonResp [new_CkJsonObject]
            CkJsonObject_Load $jsonResp [CkHttpResponse_bodyStr $resp]

            set access_token "[CkJsonObject_stringOf $jsonResp "access_token"]"
            set refresh_token "[CkJsonObject_stringOf $jsonResp "refresh_token"]"
            set id_token "[CkJsonObject_stringOf $jsonResp "id_token"]"
            set update_p [db_string update_p "select 1 from users_office365 where user_id = :user_id" -default 0]
            if {$update_p} {
                # Update
                db_dml update_access_token "update users_office365 set access_token = :access_token, refresh_token=:refresh_token, id_token=:id_token where user_id = :user_id"
            } else {
                db_dml insert_token "insert into users_office365 (user_id,access_token,refresh_token,id_token) values (:user_id,:access_token,:refresh_token,:id_token)"
            }
            delete_CkHttpResponse $resp
            delete_CkHttp $http
            delete_CkHttpRequest $req
        } else {
            # Redirect the user to the authorization instead
            ad_returnredirect "/intranet-chilkat/"
        }
    }
}
