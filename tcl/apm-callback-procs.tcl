ad_library {
    APM callback procedures.

    @creation-date 2021-06-21
    @author malte.sussdorff@cognovis.de
}

namespace eval intranet_chilkat::apm {}

ad_proc -public intranet_chilkat::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            5.0.0.0.6 5.0.0.0.7 {
                db_foreach signature {select party_id, signature from parties where signature like '%text/html%'} {
                    set html_only_sig [template::util::richtext::get_property html_value $signature]
                    db_dml update "update parties set signature = :html_only_sig where party_id = :party_id"
                }
            }
        }
}