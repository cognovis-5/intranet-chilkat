ad_proc -public -callback person_after_create -impl chilkat_email {
    -object_id:required
    { -status_id ""}
    { -type_id ""}
} {
    Add the user to the users_office365 table if they have the data enabled
} { 
    db_dml insert_user "insert into users_email (user_id) values (:object_id)"
}

ad_proc -public -callback person_before_update -impl chilkat_email {
    -object_id:required
    { -status_id ""}
    { -type_id ""}
} {
    Add the user to the users_office365 table if not already added
} { 
    set users_email_p [db_string users_email_p "select 1 from users_email where user_id = :object_id" -default 0
    if {!$users_email_p} {
        db_dml insert_user "insert into users_email (user_id) values (:object_id)"
    }
}


ad_proc -public -callback intranet_chilkat::after_sending {
    -message_id:required
    -email:required
    {-context_id ""}
    {-file_ids ""}
} {

    after sending an email with intranet_chilkat
    
    @param mesasage_id the generated message_id for this mail
    @param email CkEMail Object of the mail send
    @param context_id The ID of the object that is responsible for sending the mail in the first place
    @param file_ids IDs of the cr_files attached as we don't store them in the email
} - 

ad_proc -public -callback intranet_chilkat::failed_sending {
    -email:required
    {-context_id ""}
    {-file_ids ""}
} {

    If the sending of the email failed
    
    @param email CkEMail Object of the mail send
    @param context_id The ID of the object that is responsible for sending the mail in the first place
    @param file_ids IDs of the cr_files attached as we don't store them in the email
} - 

ad_proc -public -callback intranet_chilkat::after_sending -impl mail-tracking {
    -message_id:required
    -email:required
    {-context_id ""}
    {-file_ids ""}
} {

    after sending an email with intranet_chilkat
    
    @param mesasage_id the generated message_id for this mail
    @param email CkEMail Object of the mail send
    @param context_id The ID of the object that is responsible for sending the mail in the first place
    @param file_ids IDs of the cr_files attached as we don't store them in the email
} {

    set creation_user [auth::get_user_id]
    set creation_ip [ns_conn peeraddr]

    set package_id [acs_object::package_id -object_id $context_id]
    if {$package_id eq ""} {
        set package_id [apm_package_id_from_key "intranet-chilkat"]
    }

    set from_addr [CkEmail_fromAddress $email]
    set sender_id [party::get_by_email -email $from_addr]

    # Iterate over each TO recipient
    set num [CkEmail_get_NumTo $email]
    set to_addr_list [list]
    set to_party_ids [list]
    if {$num > 0} then {
        for {set i 0} {$i <= [expr $num - 1]} {incr i} {
            set addr [CkEmail_getToAddr $email $i]
            set party_id [party::get_by_email -email $addr]
            if {$party_id ne ""} {
                lappend to_party_ids $party_id
            } else {
                lappend to_addr_list $addr
            }
        }
    }

    # Iterate over each CC recipient
    set num [CkEmail_get_NumCC $email]
    set cc_addr_list [list]
    set cc_party_ids [list]
    if {$num > 0} then {
        for {set i 0} {$i <= [expr $num - 1]} {incr i} {
            set addr [CkEmail_getCcAddr $email $i]
            set party_id [party::get_by_email -email $addr]
            if {$party_id ne ""} {
                lappend cc_party_ids $party_id
            } else {
                lappend cc_addr_list $addr
            }
        }
    }

    # Iterate over each BCC recipient
    set num [CkEmail_get_NumBcc $email]   
    set bcc_addr_list [list]
    set bcc_party_ids [list]
    if {$num > 0} then {
        for {set i 0} {$i <= [expr $num - 1]} {incr i} {
            set addr [CkEmail_getBccAddr $email $i]
            set party_id [party::get_by_email -email $addr]
            if {$party_id ne ""} {
                lappend bcc_party_ids $party_id
            } else {
                lappend bcc_addr_list $addr
            }
        }
    }

    set body [CkEmail_getHtmlBody $email]

    set ckStr [new_CkString]
    CkEmail_get_Subject $email $ckStr 
    set subject [CkString_getString $ckStr] 
    delete_CkString $ckStr

    # the object_id passed in the API parameters is the project_id. Moreover we must assign it as context_id.
    set log_id [db_nextval "acs_object_id_seq"]

    db_exec_plsql insert_acs_mail_log {
	    SELECT acs_mail_log__new (
            :log_id,
            :message_id,
            :sender_id,
            :package_id,
            :subject,
            :body,
            :creation_user,
            :creation_ip,
            :context_id,
            :cc_addr_list,
            :bcc_addr_list,
            :to_addr_list,
            :from_addr
        )
    }

    foreach recipient_id $to_party_ids {
        db_dml insert_recipient {insert into acs_mail_log_recipient_map (recipient_id,log_id,type) values (:recipient_id,:log_id,'to')}
    }

    foreach recipient_id $cc_party_ids {
        db_dml insert_recipient {insert into acs_mail_log_recipient_map (recipient_id,log_id,type) values (:recipient_id,:log_id,'cc')}
    }

    foreach recipient_id $bcc_party_ids {
        db_dml insert_recipient {insert into acs_mail_log_recipient_map (recipient_id,log_id,type) values (:recipient_id,:log_id,'bcc')}
    }

    foreach file_id $file_ids {
		set item_id [content::revision::item_id -revision_id $file_id]
		if {$item_id eq ""} {
		    set item_id $file_id
		}
		db_dml insert_file_map "insert into acs_mail_log_attachment_map (log_id,file_id) values (:log_id,:file_id)"
    }
}

ad_proc -public -callback intranet_chilkat::failed_sending -impl chilkat_default_sender {
    -email:required
    {-context_id ""}
    {-file_ids ""}
} {

    If the sending of the email failed, try with the default sender if the from is not the default sender
    
    @param mesasage_id the generated message_id for this mail
    @param email CkEMail Object of the mail send
    @param context_id The ID of the object that is responsible for sending the mail in the first place
    @param file_ids IDs of the cr_files attached as we don't store them in the email
} {
    upvar message_id message_id

    set default_sender [parameter::get_from_package_key -package_key "intranet-chilkat" -parameter "DefaultSender" -default ""]

    if {$default_sender eq ""} {
        return ""
    }

    set from_addr [CkEmail_fromAddress $email]
    if {$default_sender eq $from_addr} {
        return ""
    }

    intranet_chilkat::mail::smtp_info -from_party_id [party::get_by_email -email $default_sender] -array_name "smtp_array"

    if {$smtp_array(smtphost) ne ""} {
		set reply_to_name [person::name -email $from_addr]
        if {$reply_to_name eq ""} {set reply_to_name $from_addr}
        CkEmail_put_ReplyTo $email "$reply_to_name <$from_addr>"
	set name [person::name -email $default_sender]
	CkEmail_put_From $email "$name <$default_sender>"

        set message_id [intranet_chilkat::mail::smtp -smtphost $smtp_array(smtphost) \
            -smtpport $smtp_array(smtpport) -smtpuser $smtp_array(smtpuser) \
            -smtppassword $smtp_array(smtppassword) -oauth2_access_token $smtp_array(oauth2_access_token) \
            -email $email
		       ]
        return "$message_id"
    } 

}

ad_proc -public -callback intranet_chilkat::after_sending -impl store_imap {
    -message_id:required
    -email:required
    {-context_id ""}
    {-file_ids ""}
} {

    Store the send email using imap
    
    @param mesasage_id the generated message_id for this mail
    @param email CkEMail Object of the mail send
    @param context_id The ID of the object that is responsible for sending the mail in the first place
    @param file_ids IDs of the cr_files attached as we don't store them in the email
} {
    set from_addr [CkEmail_fromAddress $email]
    set sender_id [party::get_by_email -email $from_addr]

    intranet_chilkat::mail::store_imap -email $email -from_party_id $sender_id
}