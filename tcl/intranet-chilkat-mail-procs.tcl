
ad_library {
	Mail procedures to work with chilkat

	@author malte.sussdorff@cognovis.de
}

ad_proc -public intranet_chilkat::send_mail {
    {-to_addr ""}
    {-to_party_ids ""}
    {-from_addr ""}
    {-from_party_id ""}
	{-cc_addr ""}
    {-cc_party_ids ""}
    {-bcc_party_ids ""}
    {-subject ""} 
    -body
    {-filesystem_files ""}
    {-file_ids ""}
	{ -object_id "" }
    { -reply_to "" }
	-fixed_sender:boolean
	-no_callback:boolean
	-no_signature:boolean
} {
    Sends an E-Mail

	@param fixed_sender Should we use the fixed sender as defined in acs-mail-lite package
	@param no_callback Should we prohibit the execution of callbacks

	@param object_id In which context do we send the E-Mail?

    @return message_id MessageID for the message
    @return err_msg In case of an error, the err_msg_list is set in the calling environment

} {	
    # This can be access by other procedures to return the errors.
    upvar err_msg_list err_msg_list
    set err_msg_list ""

    if {$from_party_id ne ""} {
        set from_addr [party::email -party_id $from_party_id]
    }

    if {$from_addr eq ""} {
        set from_addr [party::email -party_id [auth::get_user_id]]
    }

    if {$from_party_id eq ""} {
        set from_party_id [party::get_by_email -email $from_addr]
    }

    # Default reply to the from_addr
    if {$reply_to eq ""} {
        set reply_to $from_addr
    }

    #---------------------------------------------------------------
    # Handle Sender
    #---------------------------------------------------------------
    if {$fixed_sender_p} {
		# Package_id required by the callback (emmar: no idea what for)
        set mail_package_id [apm_package_id_from_key "acs-mail-lite"]
        
		# Decide which sender to use
        set fixed_sender [parameter::get -parameter "FixedSenderEmail"  -package_id $mail_package_id]

        if { $fixed_sender ne "" } {
            set from_addr $fixed_sender
        }
	}

    #---------------------------------------------------------------
    # Handle recipients
    #---------------------------------------------------------------
    set to_addr_list [list]
	foreach party_id $to_party_ids {
        lappend to_addr_list [party::email -party_id $party_id]
    }

    if {$to_addr ne ""} {
        lappend to_addr_list $to_addr
    }

    set cc_addr_list [list]
	foreach party_id $cc_party_ids {
        lappend cc_addr_list [party::email -party_id $party_id]
    }

    if {$cc_addr ne ""} {
        foreach email [join $cc_addr " "] {
            lappend cc_addr_list $email
        }
    }

    set bcc_addr_list [list]
	foreach party_id $bcc_party_ids {
        lappend bcc_addr_list [party::email -party_id $party_id]
    }


	if {!$no_signature_p} {
		set signature [db_string signature "select signature from parties where party_id = :from_party_id" -default ""]
		if {$signature ne ""} {
			append body $signature
		}
	}

	# Rollout support
    set smtp_auth_type_id ""
	set delivery_mode [parameter::get -package_id [acs_mail_lite::get_package_id] -parameter EmailDeliveryMode -default default]
	switch $delivery_mode {
		log {
			set notice "logging email instead of sending"
		}
		filter {
			set send_mode "smtp"
			set allowed_addr [parameter::get -package_id [acs_mail_lite::get_package_id] -parameter EmailAllow]

			foreach recipient [concat $to_addr_list $cc_addr_list $bcc_addr_list] {

				# if any of the recipient is not in the allowed list
				# email message has to be sent to the log instead

				if {$recipient ni $allowed_addr} {
                    set delivery_mode "log"
					set notice "logging email because one of the recipient ($recipient) is not in the EmailAllow list"
					break
				}
			}

		}
		redirect {
			# Since we have to redirect to a list of addresses
			# we need to remove the CC and BCC ones

			set to_addr [parameter::get -package_id [acs_mail_lite::get_package_id] -parameter EmailRedirectTo]
            if {$to_addr eq ""} {
                set delivery_mode "log"
                set notice "Logging mail because redirect recipient is empty"
            } else {
                set from_party_id [party::get_by_email -email $to_addr]
                if {$from_party_id eq ""} {
                    set from_party_id [auth::get_user_id]
                }
                set from_addr $to_addr
                set to_addr_list $to_addr
                set cc_addr_list ""
                set bcc_addr_list ""
            }
		}
		default {
			# proceed
		}
	}


    set email [intranet_chilkat::mail::create_CkEmail -subject $subject -body $body -to_addr_list $to_addr_list -cc_addr_list $cc_addr_list -bcc_addr_list $bcc_addr_list -from_addr $from_addr]
	intranet_chilkat::mail::append_files -email $email -filesystem_files $filesystem_files -file_ids $file_ids
    
    # If we have a different reply-to then the sender, mark this.
    if {$reply_to ne $from_addr} {
		set reply_to_name [person::name -email $reply_to]
        if {$reply_to_name eq ""} {set reply_to_name $reply_to}
        CkEmail_put_ReplyTo $email "$reply_to_name <$reply_to>"
    }

    # Logging Mails
    if {$delivery_mode eq "log"} {
        foreach err_msg $err_msg_list {
            cog_log Error $err_msg
        }
        cog_log Notice "$notice"
        cog_log Notice "intranet-chilkat::send: [CkEmail_getMime $email]"
        return "cog_log"
    }

    intranet_chilkat::mail::smtp_info -from_party_id $from_party_id -array_name "smtp_array"
    if {$smtp_array(smtphost) ne ""} {
        set message_id [intranet_chilkat::mail::smtp -smtphost $smtp_array(smtphost) \
            -smtpport $smtp_array(smtpport) -smtpuser $smtp_array(smtpuser) \
            -smtppassword $smtp_array(smtppassword) -oauth2_access_token $smtp_array(oauth2_access_token) \
            -email $email
        ]
    } else {
        set message_id ""
    }

    # Something failed, lets run callbacks to handle this.
    if {$message_id eq ""} {
        cog::callback::invoke intranet_chilkat::failed_sending -email $email -context_id $object_id -file_ids $file_ids
    } else {

        if { !$no_callback_p } {
            cog::callback::invoke intranet_chilkat::after_sending  -message_id $message_id -email $email -context_id $object_id -file_ids $file_ids
        }
    }

    foreach err_msg $err_msg_list {
        cog_log Error $err_msg
    }

	delete_CkEmail $email

	return $message_id

}

namespace eval intranet_chilkat::mail {
    
    ad_proc -public create_CkEmail {
        {-subject ""} 
        -body
        -to_addr_list:required
        {-cc_addr_list ""}
        {-bcc_addr_list ""}
        -from_addr:required
        {-reply_to ""}
    } {
        Generates the CkEMail object for sending out

        @param subject Subject of the mail
	    @param body HTML body for the email
        @param to_addr_list List of emails in the TO field
        @param to_addr_list List of emails in the CC field
        @param from_addr Sender of the email

        @return CkEMail object (which you will have to destroy / cleanup)
    } {
        upvar err_msg_list err_msg_list

        # Create a new email object
        set email [new_CkEmail]

        CkEmail_put_Subject $email [acs_mail_lite::utils::build_subject $subject]

        if {[ad_looks_like_html_p $body]} {
            set plain_body [ad_html_to_text $body]
        } else {
            set plain_body $body
            set body [ad_convert_to_html $body]
        }

        set body [encoding convertfrom [ns_encodingforcharset "UTF-8"] $body]

        CkEmail_put_Utf8 $email 1
        CkEmail_AddPlainTextAlternativeBody $email $plain_body 
        CkEmail_AddHtmlAlternativeBody $email $body


        # From user
        set from_party_id [party::get_by_email -email $from_addr]
        set name [acs_mail_lite::utils::build_subject  [person::name -person_id $from_party_id]]
        CkEmail_put_From $email "$name <$from_addr>"

        foreach to_addr $to_addr_list {
            set name  [person::name -email $to_addr]
            if {$name eq ""} {set name $to_addr}
            CkEmail_AddTo $email $name $to_addr
        }

        foreach cc_addr $cc_addr_list {
            set name [person::name -email $cc_addr]
            if {$name eq ""} {set name $cc_addr}
            CkEmail_AddCC $email $name $cc_addr
        }
        
        foreach bcc_addr $bcc_addr_list {
            set name [person::name -email $bcc_addr]
            if {$name eq ""} {set name $bcc_addr}
            CkEmail_AddBcc $email $name $bcc_addr
        }

        return $email
    }

    ad_proc -public append_files {
        -email:required
        {-filesystem_files ""}
        {-file_ids ""}
    } {
        Attach files to the email object

        @param email Object we want to attach the files to
        @param filesystem_files List of file paths we want to attach
        @param file_ids List of file_ids we want to attach

    } {
        upvar err_msg_list err_msg_list

        set revision_ids [list]
        
        # Check if we are dealing with revisions or items.
        foreach file_id $file_ids {
            set item_id [content::revision::item_id -revision_id $file_id]
            if {$item_id ne ""} {
                lappend revision_ids $file_id
            } else {
                lappend revision_ids [content::item::get_latest_revision -item_id $file_id]
            }
        }

        foreach revision_id $revision_ids {
            if {[db_0or1row file_info "select mime_type, i.name as title, content, storage_area_key from cr_revisions r, cr_items i where r.revision_id = :revision_id and i.item_id = r.item_id"]} {
                if {$content ne ""} {	
                    set file_path [cr_fs_path $storage_area_key]${content}

                    # Can't use CkEmail_AddFileAttachment as we have to specify the filename
                    set fac [new_CkFileAccess]
                    set fileBytes [new_CkByteData]
                    set success [CkFileAccess_ReadEntireFile $fac $file_path $fileBytes]

                    if {$success != 1} then {
                        set err_msg "Could not read $file_path for $title \n \n [CkFileAccess_lastErrorText $fac]"
                        lappend err_msg_list $err_msg
                        continue
                    }
                    
                    set success [CkEmail_AddDataAttachment2 $email "$title" $fileBytes "$mime_type"]
                    
                    if {$success != 1} then {
                        set err_msg "Could not attach the revision $revision_id: \n \n [CkEmail_lastErrorText $email]"
                        lappend err_msg_list $err_msg
                        # Still allow sending the mail.. maybe not smart?
                        continue
                    }
                    delete_CkFileAccess $fac
                    delete_CkByteData $fileBytes
                }
            }
        }

        foreach file $filesystem_files {
            set success [CkEmail_AddFileAttachment $email $file]
            if {$success != 1} then {
                set err_msg "Could not attach the file $file: \n \n [CkEmail_lastErrorText $email]"
                lappend err_msg_list $err_msg
                # Still allow sending the mail.. maybe not smart?
                continue
            }
        }
    }

    ad_proc -public store_imap {
        -email:required
        -from_party_id:required
    } {
        Stores the email in the IMAP folder (default is sent)

        @param email CkEMail object with the email informations
        @param folder Folder in which to store the mail.

        @return err_msg The error we recieved when trying to store the mail. Nothing if successful
    } {
        upvar err_msg_list err_msg_list
        set err_msg ""
        db_0or1row user_smtp "select imaphost, imapport, coalesce(imapuser,smtpuser) as imapuser, coalesce(imappassword,smtppassword) as imappassword, imapsentfolder from users_email where user_id = :from_party_id"
		if {$imaphost eq ""} {

			if {$imapport eq ""} {
				set imapport 993
			}

			if {$imapsentfolder eq ""} {
				set imapsentfolder "Sent"
			}

			set imap [new_CkImap]

			# Connect to an IMAP server.
			# Use TLS
			CkImap_put_Ssl $imap 1
			CkImap_put_Port $imap $imapport
            set success [CkImap_Connect $imap $imaphost]
            if {$success != 1} then {
                set err_msg "Can't connect to smtp server [CkImap_lastErrorText $imap]"
                lappend err_msg_list $err_msg
			}

			# Login
			if {$success eq 1} {
				set success [CkImap_Login $imap $imapuser $imappassword]
				if {$success != 1} then {
					set err_msg "Can't login to server $imaphost - [CkImap_lastErrorText $imap]"
                    lappend err_msg_list $err_msg
				}
			}

			if {$success eq 1} {
				# Upload (save) the email to the "Sent" mailbox.
				set success [CkImap_AppendMail $imap "Sent" $email]
				if {$success != 1} then {
					set err_msg "Can't move the mail to the send folder [CkImap_lastErrorText $imap]"
                    lappend err_msg_list $err_msg
				}
			}

			# Disconnect from the IMAP server.
			set success [CkImap_Disconnect $imap]
			delete_CkImap $imap	            
		}

        if {$err_msg ne ""} {
            return 0
        } else {
            return 1
        }
    }

    ad_proc -public smtp_info {
        -from_party_id:required
        { -array_name "smtp_array" }
    } {
        Returns the smtp information as an array using the array_name and upvar
    } {    
        upvar $array_name smtp_array
        upvar err_msg_list err_msg_list

        # Handle SMTP
        if {[db_0or1row user_smtp "select smtp_auth_type_id, smtphost, smtpport, smtpuser, smtppassword from users_email where user_id = :from_party_id"]} {
            if {$smtp_auth_type_id eq 86501} {
                if {$smtphost eq "" || $smtpuser eq "" || $smtppassword eq ""} {
                    # No host for user specific smtp authentication
                    set smtp_auth_type_id 86500
                }
            }
        } else {
            set smtp_auth_type_id 86500
        }

        # Default oauth access token
        switch $smtp_auth_type_id {
            86500 {
                # Get the SMTP Parameters
                set mail_package_id [apm_package_id_from_key "acs-mail-lite"]
                set smtphost [parameter::get -parameter "SMTPHost"  -package_id $mail_package_id  -default [ns_config ns/parameters mailhost]]
                if {$smtphost eq ""} {
                    set smtphost localhost
                }
                    
                set smtpport [parameter::get -parameter "SMTPPort"  -package_id $mail_package_id  -default 25]
                set smtpuser [parameter::get -parameter "SMTPUser"  -package_id $mail_package_id]
                set smtppassword [parameter::get -parameter "SMTPPassword"  -package_id $mail_package_id]
                set oauth2_access_token ""
            } 
            86501 {
                # We already have them from above
                set oauth2_access_token ""
            }
            86502 {
                set access_token [db_string access_token "select access_token from users_office365 where user_id = :from_party_id" -default ""]
                if {$access_token ne ""} {
                    # Refresh the token
                    intranet_chilkat::office365::refresh_token -user_id $from_party_id
                    set oauth2_access_token [db_string access_token "select access_token from users_office365 where user_id = :from_party_id" -default ""]
                    set smtpport 587
                    set smtphost "smtp.office365.com"
                    set smtpuser [party::email -party_id $from_party_id]
                } else {
                    set oauth2_access_token ""
                    set smtpport ""
                    set smtphost ""
                    set smtpuser ""
                    lappend err_msg_list "Could not find authorization for office365 for [im_name_from_id $from_party_id] ($from_party_id)."
                    if {$from_party_id eq [auth::get_user_id]} {
                        set redirect_url [intranet_chilkat::office365::oauth2_access_url]
                        lappend err_msg_list "Please <a href='$redirect_url'>login</a>"
                    }
                }
                set smtppassword ""
            }
        }
        set smtp_array(smtphost) $smtphost
        set smtp_array(smtpport) $smtpport
        set smtp_array(smtpuser) $smtpuser
        set smtp_array(smtppassword) $smtppassword
        set smtp_array(oauth2_access_token) $oauth2_access_token
    }

    ad_proc -public smtp {
        -smtphost:required
        -smtpport:required
        -smtpuser:required
        { -smtppassword ""}
        { -oauth2_access_token ""}
        -email:required
    } {
        Send E-mail using SMTP

        @param smtphost Host which will handle mail sending
        @param smtpport Port on which the host works (typicall 465 for SSL and 25 for normal non smtp
        @param smtpuser Username with which to authenticate
        @param smtppassword Password to authenticate (if required
        @param email CkEMail Object which is being send

        @return message_id Message Id which was generated
    } {
        set err_msg ""
        upvar err_msg_list err_msg_list

        # The mailman object is used for sending and receiving email.
        set mailman [new_CkMailMan]
        # Set the SMTP server.
        CkMailMan_put_SmtpHost $mailman $smtphost
        
        # Set SMTP login and password (if necessary)
        CkMailMan_put_SmtpUsername $mailman $smtpuser
        if {$smtppassword ne ""} {
            CkMailMan_put_SmtpPassword $mailman $smtppassword
        }

        if {$oauth2_access_token ne ""} {
            CkMailMan_put_OAuth2AccessToken $mailman $oauth2_access_token
        }

        switch $smtpport {
            465 {
                CkMailMan_put_SmtpSsl $mailman 1
            }
            25 {
                CkMailMan_put_SmtpSsl $mailman 0
            }
            587 {
                CkMailMan_put_StartTLS $mailman 1
            }
        }

        CkMailMan_put_SmtpPort $mailman $smtpport

        set success [CkMailMan_SendEmail $mailman $email]

        if {[expr $success != 1]} then {
	    lappend err_msg_list [CkMailMan_lastErrorText $mailman]
            if {![CkMailMan_VerifySmtpConnection $mailman]} {
                set err_msg "Could not connect to server $smtphost and post $smtpport"
            } elseif {$smtppassword == "" && $oauth2_access_token == ""} {
                set err_msg "You have to provide a password or oauth2 token"
	    } else {
                if {![CkMailMan_VerifySmtpLogin $mailman]} {
                    set err_msg "Your credentials for $smtphost with $smtpuser are wrong"
                } else {
                    set err_msg "Could not send the email: \n \n[CkMailMan_lastErrorText $mailman]"
                }
            }

            lappend err_msg_list $err_msg                    
            delete_CkMailMan $mailman
            return ""
        } 
        
        set success_close [CkMailMan_CloseSmtpConnection $mailman]
        if {[expr $success_close != 1]} then {
            set err_msg "Connection to SMTP server not closed cleanly."
            lappend err_msg_list $err_msg
            delete_CkMailMan $mailman
            return ""
        }

        set message_id [CkEmail_computeGlobalKey2 $email "base64" 0]

        delete_CkMailMan $mailman
        return $message_id
    }
}
